# Ninja Api Demo

## Made by
The following people are responsible for this product. Please contact them for any further questions.

### Team

* Iga no kuni

### Developers 
- Fujibayashi Nagato
- Momochi Sandayu

### Product owner
- Hattori Hanzo

## Getting Started

### Trust the ASP.NET Core HTTPS development certificate (Windows and macOS only):

```powershell
dotnet dev-certs https --trust # Don't execute if using docker scripts below
```

### Run manually:

```powershell
cd .\src
dotnet restore
dotnet build
dotnet run --project .\Iga.Ninja.Api\Iga.Ninja.Api.csproj
```

or just execute:

```powershell
.\build\run-local.cmd
```

### Run in docker:

```powershell
.\build\build-image.cmd # to build
.\build\run-container.cmd # to run docker container (will be available via http://localhost:8000)
```

### Run in docker with HTTPS support

```powershell
.\build\create-cert.cmd # to generate cert and configure local machine and application secrets for certificate
.\build\build-image.cmd # to build image
.\build\run-container-secure.cmd # to run over HTTPS (will be available via https://localhost:8001)
```

### Navigate with your browser to http://localhost:5000/ninja or https://localhost:5001/ninja

