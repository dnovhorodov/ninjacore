pushd %~dp0
dotnet restore ..\src\Iga.Ninja.sln
dotnet build ..\src\Iga.Ninja.sln --no-restore
dotnet run -p ..\src\Iga.Ninja.Api\Iga.Ninja.Api.csproj
popd %~dp0
